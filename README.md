This is an investigation into the calculation of quartiles, especially as produced by ggplot's geom_violin.

This repository will also be used to try MyBinder.org.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2FHywelM%2Finvestigatingquartiles/978396d47a17c467e8db130b5f1b8b58f96baef4?urlpath=rstudio)

